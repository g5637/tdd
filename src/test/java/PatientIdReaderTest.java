import org.junit.jupiter.api.Test;
import ru.makey.services.PatientIdReader;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PatientIdReaderTest {

    @Test
    void testReadPatientIdValidInput() {
        ByteArrayInputStream in = new ByteArrayInputStream("123\n".getBytes());
        Scanner scanner = new Scanner(in);
        PatientIdReader reader = new PatientIdReader(scanner);

        Integer result = reader.readPatientId();

        assertEquals(123, result);
    }

    @Test
    void testReadPatientIdInvalidInput() {
        ByteArrayInputStream in = new ByteArrayInputStream("abc\n".getBytes());
        Scanner scanner = new Scanner(in);
        PatientIdReader reader = new PatientIdReader(scanner);

        Integer result = reader.readPatientId();

        assertNull(result);
    }
}
