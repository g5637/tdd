package executors;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatus;
import ru.makey.services.PatientIdReader;
import ru.makey.services.executors.GetStatusCommandExecutor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetStatusCommandExecutorTest {

    private static Hospital hospital;
    private static PatientIdReader patientIdReader;
    private static ByteArrayOutputStream fakeOutput;

    @BeforeAll
    static void setUp() {
        hospital = mock(Hospital.class);
        patientIdReader = mock(PatientIdReader.class);
        fakeOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(fakeOutput));
    }

    @BeforeEach
    void beforeEach() {
        Mockito.reset(hospital, patientIdReader);
    }

    @Test
    void shouldPrintStatusOfPatientWhenPatientExists() {
        // Arrange
        Patient patient = new Patient(1, PatientStatus.SICK);
        when(patientIdReader.readPatientId()).thenReturn(1);
        when(hospital.findPatientById(anyInt())).thenReturn(Optional.of(patient));
        GetStatusCommandExecutor executor = new GetStatusCommandExecutor(hospital, patientIdReader);

        // Act
        executor.exec();

        // Assert
        Assertions.assertTrue(fakeOutput.toString().contains("Статус пациента: Болен"));
    }

    @Test
    void shouldPrintErrorMessageWhenPatientDoesNotExist() {
        // Arrange
        when(hospital.findPatientById(anyInt())).thenReturn(Optional.empty());
        GetStatusCommandExecutor executor = new GetStatusCommandExecutor(hospital, patientIdReader);

        // Act
        executor.exec();

        // Assert
        Assertions.assertTrue(fakeOutput.toString().contains("Ошибка. В больнице нет пациента с таким ID"));
    }

    @AfterAll
    static void tearDown() {
        System.setOut(System.out);
    }
}