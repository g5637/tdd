import org.junit.jupiter.api.Test;
import ru.makey.services.WriteOutQuestionMaker;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WriteOutQuestionMakerTest {

    @Test
    void testAskYes() {
        ByteArrayInputStream in = new ByteArrayInputStream("да\n".getBytes());
        Scanner scanner = new Scanner(in);
        WriteOutQuestionMaker questionMaker = new WriteOutQuestionMaker(scanner);

        boolean result = questionMaker.ask();

        assertTrue(result);
    }

    @Test
    void testAskInvalidInput() {
        ByteArrayInputStream in = new ByteArrayInputStream("abc\n".getBytes());
        Scanner scanner = new Scanner(in);
        WriteOutQuestionMaker questionMaker = new WriteOutQuestionMaker(scanner);

        boolean result = questionMaker.ask();

        assertFalse(result);
    }
}
