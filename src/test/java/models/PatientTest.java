package models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatus;

public class PatientTest {

    @Test
    public void statusUpTest() {
        Patient patient = new Patient(1, PatientStatus.SICK);
        patient.stuatusUp();
        Assertions.assertEquals(patient.getStatus(), PatientStatus.LIGHTSICK);
    }

    @Test
    public void statusDownTest() {
        Patient patient = new Patient(1, PatientStatus.SICK);
        patient.statusDown();
        Assertions.assertEquals(patient.getStatus(), PatientStatus.HARDSICK);
    }


    @Test
    public void statusUpMaxTest() {
        Patient patient = new Patient(1, PatientStatus.HEALTHY);
        Assertions.assertThrows(IndexOutOfBoundsException.class, patient::stuatusUp);
    }

    @Test
    public void statusDownMinTest() {
        Patient patient = new Patient(1, PatientStatus.HARDSICK);
        Assertions.assertThrows(IndexOutOfBoundsException.class, patient::statusDown);
    }

    @Test
    public void isWriteoutReadyTrueTest() {
        Patient patient = new Patient(1, PatientStatus.HEALTHY);
        Assertions.assertTrue(patient.isWriteoutReady());
    }

    @Test
    public void isWriteoutReadyFalseTest() {
        Patient patient = new Patient(1, PatientStatus.SICK);
        Assertions.assertFalse(patient.isWriteoutReady());
    }

}
