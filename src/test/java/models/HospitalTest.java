package models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatus;
import ru.makey.models.patient.PatientStatusException;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class HospitalTest {

    public final Hospital hospital = new Hospital(new ArrayList<>(IntStream.range(1, 201)
            .mapToObj(Patient::new)
            .toList()));

    @Test
    public void findPatientOutOfRange() {
        Assertions.assertFalse(hospital.findPatientById(0).isPresent());
        Assertions.assertFalse(hospital.findPatientById(201).isPresent());
    }

    @Test
    public void findPatientSuccess() {
        Assertions.assertTrue(hospital.findPatientById(1).isPresent());
        Assertions.assertTrue(hospital.findPatientById(200).isPresent());
    }

    @Test
    public void upPatientStatusSuccess() {
        Patient patient = new Patient(1, PatientStatus.SICK);
        Assertions.assertDoesNotThrow(patient::stuatusUp);
        Assertions.assertEquals(patient.getStatus(), PatientStatus.LIGHTSICK);
    }

    @Test
    public void upPatientStatusWithException() {
        Patient patient = new Patient(1, PatientStatus.HEALTHY);
        Assertions.assertThrows(PatientStatusException.class, () -> hospital.upPatientStatus(patient));
        Assertions.assertEquals(patient.getStatus(), PatientStatus.HEALTHY);
    }

    @Test
    public void downPatientStatusSuccess() {
        Patient patient = new Patient(1, PatientStatus.SICK);
        Assertions.assertDoesNotThrow(() -> hospital.downPatientStatus(patient));
        Assertions.assertEquals(patient.getStatus(), PatientStatus.HARDSICK);
    }

    @Test
    public void downPatientStatusWithException() {
        Patient patient = new Patient(1, PatientStatus.HARDSICK);
        Assertions.assertThrows(PatientStatusException.class, () -> hospital.downPatientStatus(patient));
        Assertions.assertEquals(patient.getStatus(), PatientStatus.HARDSICK);
    }

    @Test
    public void writeOutPatientTest() {
        Patient patient = this.hospital.patientList().get(0);
        hospital.writeOutPatient(patient);
        Assertions.assertFalse(hospital.patientList().contains(patient));
    }


}
