package ru.makey.models;

public enum Command {
    GetStatus,
    CalculateStat,
    StatusUp,
    StatusDown,
    Stop,
    Unknown;

    public static Command fromString(String c) {
        return switch (c) {
            case "get status", "узнать статус" -> GetStatus;
            case "status up", "увеличить статус" -> StatusUp;
            case "status down", "понизить статус" -> StatusDown;
            case "calculate stats", "рассчитать статистику" -> CalculateStat;
            case "stop", "стоп" -> Stop;
            default -> Unknown;
        };
    }
}
