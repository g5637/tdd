package ru.makey.models.patient;

public class Patient {

    private final int id;

    private PatientStatus status;

    public Patient(int id, PatientStatus status) {
        this.id = id;
        this.status = status;
    }

    public Patient(int id) {
        this(id, PatientStatus.SICK);
    }

    public int getId() {
        return id;
    }

    public PatientStatus getStatus() {
        return status;
    }

    public void stuatusUp() {
        this.status = PatientStatus.fromInt(this.status.ordinal() + 1);
    }

    public void statusDown() {
        this.status = PatientStatus.fromInt(this.status.ordinal() - 1);
    }

    public boolean isWriteoutReady() {
        return this.status == PatientStatus.HEALTHY;
    }
}
