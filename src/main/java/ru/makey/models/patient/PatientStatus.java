package ru.makey.models.patient;

public enum PatientStatus {
    HARDSICK("Тяжело болен"),
    SICK("Болен"),
    LIGHTSICK("Слегка болен"),
    HEALTHY("Готов к выписке");

    private final String statusName;

    PatientStatus(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public String toString() {
        return statusName;
    }

    public static PatientStatus fromInt(Integer i) {
        return PatientStatus.values()[i];
    }
}
