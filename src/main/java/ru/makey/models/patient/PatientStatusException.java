package ru.makey.models.patient;

public class PatientStatusException extends Exception {
    public PatientStatusException(String message) {
        super(message);
    }
}
