package ru.makey.models;

import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatusException;

import java.util.List;
import java.util.Optional;

public record Hospital(List<Patient> patientList) {

    public Optional<Patient> findPatientById(Integer id) {
        return this.patientList.stream()
                .filter(p -> p.getId() == id)
                .findFirst();
    }

    public void upPatientStatus(Patient patient) throws PatientStatusException {
        try {
            patient.stuatusUp();
        } catch (IndexOutOfBoundsException ex) {
            throw new PatientStatusException("Ошибка. Нельзя понизить самый низкий статус (наши пациенты не умирают)");
        }
    }

    public void downPatientStatus(Patient patient) throws PatientStatusException {
        try {
            patient.statusDown();
        } catch (IndexOutOfBoundsException ex) {
            throw new PatientStatusException("Ошибка. Нельзя повысить самый высокий статус (пора выписываться)");
        }
    }

    public void writeOutPatient(Patient patient) {
        this.patientList.remove(patient);
    }
}
