package ru.makey.services;

import java.util.Scanner;

public class PatientIdReader {

    private final Scanner scanner;

    public PatientIdReader(Scanner mainIn) {
        this.scanner = mainIn;
    }


    public Integer readPatientId() {
        System.out.print("Введите ID пациента: ");
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (Exception ex) {
            System.out.println("Ошибка. ID пациента должно быть числом (целым, положительным)" + ex.getMessage());
            return null;
        }
    }

}
