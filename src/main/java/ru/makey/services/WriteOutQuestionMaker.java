package ru.makey.services;

import java.util.Scanner;

public class WriteOutQuestionMaker {

    private final Scanner scanner;

    public WriteOutQuestionMaker(Scanner mainIn) {
        scanner = mainIn;
    }


    public boolean ask() {
        System.out.print("Желаете этого клиента выписать? (да/Нет): ");
        String s = scanner.nextLine();
        return switch (s) {
            case "да" -> true;
            case "нет" -> false;
            default -> false;
        };
    }

}
