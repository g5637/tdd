package ru.makey.services.executors;

import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.services.PatientIdReader;

public class GetStatusCommandExecutor extends PatientWithIdCommandExecutor {

    public GetStatusCommandExecutor(Hospital hospital, PatientIdReader patientIdReader) {
        super(hospital, patientIdReader);
    }

    @Override
    protected void execWithPatient(Patient patient) {
        System.out.println("Статус пациента: " + patient.getStatus());
    }

}
