package ru.makey.services.executors;

import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.services.PatientIdReader;

abstract class PatientWithIdCommandExecutor extends PatientCommandExecutor {

    private final PatientIdReader patientIdReader;

    PatientWithIdCommandExecutor(Hospital hospital, PatientIdReader patientIdReader) {
        super(hospital);
        this.patientIdReader = patientIdReader;
    }

    @Override
    public void exec() {
        Integer id = patientIdReader.readPatientId();
        if (id != null)
            hospital.findPatientById(id).ifPresentOrElse(
                    this::execWithPatient,
                    () -> System.out.println("Ошибка. В больнице нет пациента с таким ID")
            );
    }

    protected abstract void execWithPatient(Patient patient);

}
