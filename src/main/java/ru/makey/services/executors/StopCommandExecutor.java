package ru.makey.services.executors;

public class StopCommandExecutor implements CommandExecutor {

    @Override
    public void exec() {
        System.out.println("Сеанс завершён.");
    }

}
