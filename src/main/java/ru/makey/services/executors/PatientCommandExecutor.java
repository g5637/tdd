package ru.makey.services.executors;

import ru.makey.models.Hospital;

abstract class PatientCommandExecutor implements CommandExecutor {

    protected final Hospital hospital;

    PatientCommandExecutor(Hospital hospital) {
        this.hospital = hospital;
    }
}
