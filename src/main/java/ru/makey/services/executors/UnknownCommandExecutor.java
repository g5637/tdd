package ru.makey.services.executors;

public class UnknownCommandExecutor implements CommandExecutor {
    @Override
    public void exec() {
        System.out.println("Ошибка! Незивестная команда. Попробуйте еще раз");
    }

}
