package ru.makey.services.executors;

public interface CommandExecutor {

    void exec();
}
