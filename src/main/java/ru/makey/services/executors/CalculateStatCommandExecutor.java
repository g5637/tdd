package ru.makey.services.executors;

import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatus;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CalculateStatCommandExecutor extends PatientCommandExecutor {

    public CalculateStatCommandExecutor(Hospital hospital) {
        super(hospital);
    }

    @Override
    public void exec() {
        List<Patient> patients = hospital.patientList();

        Map<PatientStatus, List<Patient>> stats = patients.stream()
                .sorted(Comparator.comparing(p -> p.getStatus().ordinal(), Comparator.reverseOrder()))
                .collect(Collectors.groupingBy(Patient::getStatus));

        StringBuilder resultMessage = new StringBuilder();
        resultMessage
                .append("В больнице на данный момент находится ")
                .append(patients.size())
                .append(" чел., из них:\n");

        stats.forEach((k, v) -> resultMessage
                .append("   - в статусе \"")
                .append(k.toString())
                .append("\": ")
                .append(v.size())
                .append(" чел.\n"));

        System.out.println(resultMessage);
    }

}
