package ru.makey.services.executors;

import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatusException;
import ru.makey.services.PatientIdReader;

public class StatusDownCommandExecutor extends PatientWithIdCommandExecutor {

    public StatusDownCommandExecutor(Hospital hospital, PatientIdReader patientIdReader) {
        super(hospital, patientIdReader);
    }

    @Override
    protected void execWithPatient(Patient patient) {
        try {
            this.hospital.downPatientStatus(patient);
            System.out.println("Новый статус пациента: " + patient.getStatus());
        } catch (PatientStatusException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
