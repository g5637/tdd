package ru.makey.services.executors;

import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.models.patient.PatientStatusException;
import ru.makey.services.PatientIdReader;
import ru.makey.services.WriteOutQuestionMaker;

public class StatusUpCommandExecutor extends PatientWithIdCommandExecutor {

    private final WriteOutQuestionMaker writeOutQuestionMaker;

    public StatusUpCommandExecutor(Hospital hospital, PatientIdReader patientIdReader, WriteOutQuestionMaker writeOutQuestionMaker) {
        super(hospital, patientIdReader);
        this.writeOutQuestionMaker = writeOutQuestionMaker;
    }

    @Override
    protected void execWithPatient(Patient patient) {
        if (patient.isWriteoutReady())
            if (writeOutQuestionMaker.ask()) {
                hospital.writeOutPatient(patient);
                System.out.println("Пациент выписан из больницы");
            } else {
                System.out.println("Пациент остался в статусе " + patient.getStatus());
            }
        else {
            try {
                this.hospital.upPatientStatus(patient);
                System.out.println("Новый статус пациента: " + patient.getStatus());
            } catch (PatientStatusException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
