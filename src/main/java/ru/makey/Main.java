package ru.makey;

import ru.makey.models.Command;
import ru.makey.models.Hospital;
import ru.makey.models.patient.Patient;
import ru.makey.services.PatientIdReader;
import ru.makey.services.WriteOutQuestionMaker;
import ru.makey.services.executors.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public static final Hospital hospital = new Hospital(new ArrayList<>(IntStream.range(1, 201)
            .mapToObj(Patient::new)
            .toList()));
    private static final Scanner scanner = new Scanner(System.in);

    private static final PatientIdReader patientIdReader = new PatientIdReader(scanner);

    private static final WriteOutQuestionMaker writeOutQuestionMaker = new WriteOutQuestionMaker(scanner);

    public static final Map<Command, CommandExecutor> executorMap = Map.of(
            Command.StatusUp, new StatusUpCommandExecutor(hospital, patientIdReader, writeOutQuestionMaker),
            Command.GetStatus, new GetStatusCommandExecutor(hospital, patientIdReader),
            Command.StatusDown, new StatusDownCommandExecutor(hospital, patientIdReader),
            Command.CalculateStat, new CalculateStatCommandExecutor(hospital),
            Command.Stop, new StopCommandExecutor(),
            Command.Unknown, new UnknownCommandExecutor()
    );

    public static void main(String[] args) {
        Command command;
        do {
            System.out.print("Введите команду: ");
            command = Command.fromString(scanner.nextLine());
            executorMap.get(command).exec();
        } while (command != Command.Stop);
    }
}